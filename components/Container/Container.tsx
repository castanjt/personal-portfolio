import { Component, FunctionComponent } from 'react';
import { AppProps } from 'interfaces/app.interface';

export function WithContainer(WrappedComponent: FunctionComponent): any {
    return class extends Component<AppProps> {
        constructor(props) {
            super(props);
        }

        render() {
            const siteName = process.env.SITE_NAME;
            const pageProps = {
                ...this.props,
                siteName,
            };

            return <WrappedComponent {...pageProps} />;
        }
    };
}
