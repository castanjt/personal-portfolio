import { FunctionComponent } from 'react';
import styles from 'components/Footer/Footer.module.scss';

const Footer: FunctionComponent<{
    pageProps: any;
}> = ({ pageProps }): JSX.Element => {
    const date = new Date();
    return (
        <footer className={styles.footer}>
            <div className={'container'}>
                <div className={styles.content}>
                    <span>
                        {pageProps.siteName} &copy; {date.getFullYear()}
                    </span>
                    <span className={styles.copyright}>
                        All rights reserved
                    </span>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
