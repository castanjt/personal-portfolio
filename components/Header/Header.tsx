import { FunctionComponent } from 'react';
import Link from 'next/link';
import Navigation from 'components/Navigation/Navigation';

import styles from 'components/Header/Header.module.scss';

const Header: FunctionComponent<{
    pageProps: any;
}> = ({ pageProps }): JSX.Element => {
    return (
        <header className={styles.header}>
            <div className={'container'}>
                <Navigation pageProps={pageProps} />
                <div className={styles.hero}>
                    <div className={styles.siteName}>
                        <Link href="/">
                            <a>{pageProps.siteName}</a>
                        </Link>
                        <div className={styles.ellipsis}>···</div>
                    </div>
                    <div className={styles.tagline}>
                        name: Jeremy Castanza <br />
                        occupation: Full Stack Developer
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
