import { FunctionComponent } from 'react';
import Link from 'next/link';
import styles from 'components/Navigation/Navigation.module.scss';

const Navigation: FunctionComponent<{
    pageProps: any;
}> = (): JSX.Element => {
    return (
        <nav className={styles.navigation}>
            <ul>
                <li>
                    <Link href="/portfolio">
                        <a>Portfolio</a>
                    </Link>
                </li>
                <li>
                    <Link href="/about">
                        <a>About</a>
                    </Link>
                </li>
                <li>
                    <Link href="/skills">
                        <a>Skills</a>
                    </Link>
                </li>
                <li>
                    <Link href="/contact">
                        <a>Contact</a>
                    </Link>
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;
