import Head from 'next/head';
import { useRouter } from 'next/router';
import { FunctionComponent } from 'react';
import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import PageHeader from 'components/PageHeader/PageHeader';
import styles from 'components/Layout/Layout.module.scss';

const Layout: FunctionComponent<{
    children: Record<string, any>;
    pageProps: any;
}> = ({ children, pageProps }): JSX.Element => {
    const router = useRouter();
    const header =
        router.pathname === '/' ? (
            <Header pageProps={pageProps} />
        ) : (
            <PageHeader pageProps={pageProps} />
        );
    return (
        <>
            <Head>
                <title />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            {header}
            <div className={'container'}>
                <main className={styles.main}>{children}</main>
            </div>
            <Footer pageProps={pageProps} />
        </>
    );
};

export default Layout;
