import { FunctionComponent } from 'react';
import Link from 'next/link';
import Navigation from 'components/Navigation/Navigation';

import styles from 'components/PageHeader/PageHeader.module.scss';

const PageHeader: FunctionComponent<{
    pageProps: any;
}> = ({ pageProps }): JSX.Element => {
    return (
        <header className={styles.pageHeader}>
            <div className={'container'}>
                <div className={styles.container}>
                    <Link href="/">
                        <a className={styles.siteName}>{pageProps.siteName}</a>
                    </Link>
                    <Navigation pageProps={pageProps} />
                </div>
            </div>
        </header>
    );
};

export default PageHeader;
