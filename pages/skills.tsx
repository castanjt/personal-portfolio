import { FunctionComponent } from 'react';
import Head from 'next/head';
import { AppProps } from 'interfaces/app.interface';
import { WithContainer } from 'components/Container/Container';
import Layout from 'components/Layout/Layout';
import { Card } from 'primereact/card';

import styles from 'styles/pages/skills.module.scss';

const Skills: FunctionComponent<AppProps> = (pageProps): JSX.Element => {
    const CardHeader = ({ url }) => (
        <div className={styles.cardHeader}>
            <img
                alt="Card"
                src={url}
                onError={(e) => {
                    (e.target as HTMLInputElement).src =
                        'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png';
                }}
            />
        </div>
    );

    return (
        <Layout pageProps={pageProps}>
            <Head>
                <title>Jeremy Castanza | Skills</title>
            </Head>
            <section className={'pageHeader'}>
                <h1>Skills</h1>
            </section>
            <section className={styles.cards}>
                <Card
                    title="Programming"
                    subTitle="Experienced programming with:"
                    className={styles.card}
                    header={<CardHeader url={'ts.png'} />}
                >
                    <ul className={styles.list}>
                        <li>Typescript</li>
                        <li>Javascript</li>
                        <li>PHP</li>
                        <li>HTML/CSS</li>
                        <li>SCSS/Sass</li>
                        <li>GraphQL</li>
                        <li>SQL</li>
                    </ul>
                </Card>
                <Card
                    title="Frameworks/Libraries"
                    subTitle="Typical frameworks/libraries used:"
                    className={styles.card}
                    header={<CardHeader url={'react.png'} />}
                >
                    <ul className={styles.list}>
                        <li>React</li>
                        <li>Redux</li>
                        <li>NestJS</li>
                        <li>Next.js</li>
                        <li>Laravel</li>
                        <li>Bootstrap</li>
                    </ul>
                </Card>
                <Card
                    title="CMS's"
                    subTitle="Tons of experience with:"
                    className={styles.card}
                    header={<CardHeader url={'wp.png'} />}
                >
                    <ul className={styles.list}>
                        <li>WordPress</li>
                        <li>Drupal</li>
                        <li>Joomla</li>
                        <li>Ghost</li>
                        <li>Headless</li>
                    </ul>
                </Card>
                <Card
                    title="Server-side Tech"
                    subTitle="Skillful with:"
                    className={styles.card}
                    header={<CardHeader url={'docker.png'} />}
                >
                    <ul className={styles.list}>
                        <li>Apache</li>
                        <li>Nginx</li>
                        <li>Node/Express</li>
                        <li>Docker</li>
                        <li>VirtualBox/Vagrant</li>
                    </ul>
                </Card>
                <Card
                    title="Hosts"
                    subTitle="Proficient in powering with:"
                    className={styles.card}
                    header={<CardHeader url={'aws.png'} />}
                >
                    <ul className={styles.list}>
                        <li>AWS (EC2, Lightsail, and Lambda)</li>
                        <li>Azure</li>
                        <li>Siteground</li>
                        <li>Kinsta</li>
                        <li>Pantheon</li>
                        <li>Netlify</li>
                        <li>GoDaddy</li>
                    </ul>
                </Card>
                <Card
                    title="DevTools"
                    subTitle="My toolbox:"
                    className={styles.card}
                    header={<CardHeader url={'vscode.png'} />}
                >
                    <ul className={styles.list}>
                        <li>MacBook Pro</li>
                        <li>Visual Studio Code</li>
                        <li>WebStorm/PHPStorm</li>
                        <li>Adobe Photoshop</li>
                        <li>Jira</li>
                        <li>Bitbucket</li>
                        <li>Terminal</li>
                        <li>Github</li>
                        <li>Google Chrome</li>
                        <li>DBeaver</li>
                        <li>TablePlus</li>
                        <li>Insomnia</li>
                        <li>Postman</li>
                        <li>Zoom</li>
                        <li>Slack</li>
                        <li>OpenVPN</li>
                        <li>BrowserStack</li>
                        <li>FileZilla</li>
                        <li>SSH</li>
                        <li>Let&apos;s Encrypt</li>
                        <li>StackOverflow</li>
                    </ul>
                </Card>
            </section>
        </Layout>
    );
};

export default WithContainer(Skills);
