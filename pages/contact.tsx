import Head from 'next/head';
import Layout from 'components/Layout/Layout';
import { useFormik } from 'formik';
import { FunctionComponent, useState } from 'react';
import { AppProps } from 'interfaces/app.interface';
import { WithContainer } from 'components/Container/Container';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Button } from 'primereact/button';
import { validateFieldSync } from 'common/validation/validateField';

import styles from 'styles/pages/contact.module.scss';

const Contact: FunctionComponent<AppProps> = (pageProps): JSX.Element => {
    const [sent, setSent] = useState(false);
    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            message: '',
        },

        validate: (data) => {
            const errors: any = {};

            const nameValidation = validateFieldSync('name', data.name);
            if (!nameValidation.isValid) {
                errors.name = 'Please enter name.';
            }

            const emailValidation = validateFieldSync('email', data.email);
            if (!emailValidation.isValid) {
                errors.email = 'Please enter a valid email address.';
            }

            const messageValidation = validateFieldSync(
                'message',
                data.message,
            );
            if (!messageValidation.isValid) {
                errors.message = 'Please enter a complete message.';
            }

            return errors;
        },

        onSubmit: (data) => {
            sendMail(data);
            setSent(true);
            formik.resetForm();
        },
    });

    const sendMail = async (data) => {
        await fetch('/.netlify/functions/contact', {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(data),
        });
    };

    const isFormFieldValid = (name) =>
        !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return (
            isFormFieldValid(name) && (
                <small className="p-error">{formik.errors[name]}</small>
            )
        );
    };

    return (
        <Layout pageProps={pageProps}>
            <Head>
                <title>Jeremy Castanza | Contact</title>
            </Head>
            <div className={styles.container}>
                <article className={styles.article}>
                    <header className={'pageHeader'}>
                        <h1>Contact</h1>
                    </header>
                    <section className={styles.content}>
                        {sent ? (
                            <div>
                                <h2>Thank you!</h2>
                                <p>
                                    Your message has been sent. As soon as I see
                                    it, we will be in touch.
                                </p>
                            </div>
                        ) : (
                            <form onSubmit={formik.handleSubmit}>
                                <div className={styles.inputGroup}>
                                    <label htmlFor={null}>Name:</label>
                                    <InputText
                                        name={'name'}
                                        className={styles.input}
                                        value={formik.values.name}
                                        placeholder={'Please enter your name'}
                                        onChange={formik.handleChange}
                                    />
                                    <div className={styles.error}>
                                        {getFormErrorMessage('name')}
                                    </div>
                                </div>
                                <div className={styles.inputGroup}>
                                    <label htmlFor={null}>Email:</label>
                                    <InputText
                                        name={'email'}
                                        className={styles.input}
                                        value={formik.values.email}
                                        placeholder={'Please enter your email'}
                                        onChange={formik.handleChange}
                                    />
                                    <div className={styles.error}>
                                        {getFormErrorMessage('email')}
                                    </div>
                                </div>
                                <div className={styles.inputGroup}>
                                    <label htmlFor={null}>Message:</label>
                                    <InputTextarea
                                        name={'message'}
                                        className={styles.input}
                                        value={formik.values.message}
                                        placeholder={'Enter your message here'}
                                        onChange={formik.handleChange}
                                    />
                                    <div className={styles.error}>
                                        {getFormErrorMessage('message')}
                                    </div>
                                </div>
                                <Button type="submit">Send Message</Button>
                            </form>
                        )}
                    </section>
                </article>
            </div>
            <footer className={styles.scripture}>
                <p>
                    &quot;Therefore, whether you eat or drink, or whatever you
                    do, do all things for the glory of God.&quot; -{' '}
                    <strong>1 Corinthians 10:21</strong>
                </p>
            </footer>
        </Layout>
    );
};

export default WithContainer(Contact);
