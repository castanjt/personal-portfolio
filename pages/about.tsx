import Head from 'next/head';
import { FunctionComponent } from 'react';
import { AppProps } from 'interfaces/app.interface';
import { WithContainer } from 'components/Container/Container';
import Layout from 'components/Layout/Layout';

import styles from 'styles/pages/about.module.scss';

const About: FunctionComponent<AppProps> = (pageProps): JSX.Element => {
    return (
        <Layout pageProps={pageProps}>
            <Head>
                <title>Jeremy Castanza | About</title>
            </Head>
            <div className={styles.container}>
                <article className={styles.article}>
                    <header className={'pageHeader'}>
                        <h1>About</h1>
                    </header>
                    <section className={styles.content}>
                        <p>
                            I&apos;m a full-stack web developer with eight years
                            of programming experience. I mostly enjoy working in
                            Typescript and Javascript. I also have extensive
                            experience with PHP and have dabbled in Visual
                            Basic.Net and Java.
                        </p>
                        <p>
                            My love for development stems from a desire to build
                            things. I enjoy creating things that help other
                            people. I have a passion for UX and sustainable,
                            semantic code. From a work perspective, I enjoy
                            peer-to-peer programming and working collaboratively
                            in small teams using agile.
                        </p>
                        <p>
                            When I&apos;m not coding, I enjoy spending time by
                            going on adventures with my wife. Our favorite
                            pastimes are re-decorating, biking, kayaking,
                            hiking, and road tripping. I also enjoy woodworking,
                            learning guitar, and reading.
                        </p>
                    </section>
                </article>
                <aside className={styles.photo}>
                    <div className={styles.overlay} />
                    <img
                        alt={'Jeremy at Bryce Canyon with his wife'}
                        src={'jeremy.jpg'}
                    />
                </aside>
            </div>
            <footer className={styles.scripture}>
                <p>
                    &quot;Therefore, whether you eat or drink, or whatever you
                    do, do all things for the glory of God.&quot; -{' '}
                    <strong>1 Corinthians 10:21</strong>
                </p>
            </footer>
        </Layout>
    );
};

export default WithContainer(About);
