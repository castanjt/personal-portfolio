import { FunctionComponent, useEffect, useRef } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { AppProps } from 'interfaces/app.interface';
import { WithContainer } from 'components/Container/Container';
import Layout from 'components/Layout/Layout';

import { Terminal } from 'primereact/terminal';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { TerminalService } from 'primereact/terminalservice';

import styles from 'styles/pages/index.module.scss';

const Home: FunctionComponent<AppProps> = (pageProps): JSX.Element => {
    const router = useRouter();
    const terminalRef = useRef(null);

    const setFocus = () => {
        const terminal = terminalRef.current;
        terminal && terminal.input.focus();
    };

    const commandList = [
        {
            command: 'view portfolio',
            description: `See Jeremy's portfolio.`,
        },
        {
            command: 'view skills',
            description: `See Jeremy's skills.`,
        },
        {
            command: 'view about',
            description: `Learn about Jeremy.`,
        },
        {
            command: 'view contact',
            description: `Open contact page.`,
        },
        {
            command: 'programs',
            description: `View programming experience in terminal.`,
        },
        {
            command: 'hobbies',
            description: `See Jeremy's hobbies in terminal.`,
        },
        {
            command: 'email',
            description: `See Jeremy's email address.`,
        },
        {
            command: 'contact',
            description: `Shorthand to open contact form.`,
        },
        {
            command: 'portfolio',
            description: `Shorthand to see Jeremy's portfolio.`,
        },
        {
            command: 'skills',
            description: `Shorthand to view skills..`,
        },
        {
            command: 'about',
            description: `Shorthand to view about.`,
        },
        {
            command: 'date',
            description: `View current date.`,
        },
        {
            command: 'clear',
            description: `Clear all results in terminal.`,
        },
    ];

    const commandHandler = (text) => {
        let response;
        const argsIndex = text.indexOf(' ');
        const command = argsIndex !== -1 ? text.substring(0, argsIndex) : text;

        switch (command) {
            case 'help': {
                response = (
                    <div>
                        Here are a list of commands that you can type:
                        {commandList.map((data, index) => (
                            <div key={index}>
                                <span className={styles.command}>
                                    {data.command} -
                                </span>{' '}
                                {data.description}
                            </div>
                        ))}
                    </div>
                );
                break;
            }
            case 'view': {
                const route = text.substring(argsIndex + 1);
                if (
                    route === 'skills' ||
                    route === 'portfolio' ||
                    route === 'about' ||
                    route === 'contact'
                )
                    router.push(`/${route}`);
                else response = `Sorry, that page or command is unavailable.`;
                break;
            }
            case 'about': {
                router.push('/about');
                break;
            }
            case 'portfolio': {
                router.push('/portfolio');
                break;
            }
            case 'skills': {
                router.push('/skills');
                break;
            }
            case 'contact': {
                router.push('/contact');
                break;
            }
            case 'date': {
                response = 'Today is ' + new Date().toDateString();
                break;
            }
            case 'email': {
                response = `My email address is: jcastanza.me@gmail.com`;
                break;
            }
            case 'programs': {
                response = `I work well with Typescript and React, as well as several other tools. Type skills to learn more.`;
                break;
            }
            case 'hobbies': {
                response = `I enjoy biking, kayaking, hiking, and road tripping.`;
                break;
            }
            case 'clear': {
                response = null;
                break;
            }
            default: {
                response = 'Unknown command: ' + command;
                break;
            }
        }

        if (response) {
            TerminalService.emit('response', response);
        } else {
            TerminalService.emit('clear');
        }
    };

    useEffect(() => {
        setFocus();
        TerminalService.on('command', commandHandler);

        return () => {
            TerminalService.off('command', commandHandler);
        };
    }, []);

    return (
        <Layout pageProps={pageProps}>
            <Head>
                <title>Jeremy Castanza | Full Stack Developer</title>
            </Head>
            <div className={styles.container}>
                <aside className={styles.sidebar}>
                    <DataTable
                        value={commandList}
                        className={styles.commandList}
                        header={'List of commands'}
                    >
                        <Column field="command" header="Command"></Column>
                        <Column
                            field="description"
                            header="Description"
                        ></Column>
                    </DataTable>
                </aside>
                <article className={styles.terminal}>
                    <h1>Hello World!</h1>
                    <p>
                        My name is Jeremy Castanza and I&apos;m a Full Stack
                        Developer. Use the terminal below to learn more about me
                        and my work. For a list of commands, check out the
                        sidebar or type `help` in the black box below.
                    </p>
                    <div className="terminal">
                        <Terminal
                            ref={terminalRef}
                            welcomeMessage="Let's get to work. Type 'help' to learn more."
                            prompt="jcastanza.me $"
                        />
                    </div>
                </article>
            </div>
        </Layout>
    );
};

export default WithContainer(Home);
