import { FunctionComponent } from 'react';
import Head from 'next/head';
import { AppProps } from 'interfaces/app.interface';
import { WithContainer } from 'components/Container/Container';
import Layout from 'components/Layout/Layout';
import { Card } from 'primereact/card';

import styles from 'styles/pages/portfolio.module.scss';

const Portfolio: FunctionComponent<AppProps> = (pageProps): JSX.Element => {
    const CardHeader = ({ url, style }) => (
        <div className={styles.cardHeader}>
            <img
                alt="Card"
                src={url}
                onError={(e) => {
                    (e.target as HTMLInputElement).src =
                        'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png';
                }}
                style={style ? style : null}
            />
        </div>
    );

    return (
        <Layout pageProps={pageProps}>
            <Head>
                <title>Jeremy Castanza | Portfolio</title>
            </Head>
            <section className={'pageHeader'}>
                <h1>Portfolio</h1>
                <p>
                    Throughout my career, I&apos;ve worked on a variety of
                    software and websites for small, mid-size, and large
                    businesses, government agencies, and universities. Here are
                    some brands that I&apos;ve helped to build websites or
                    software for:
                </p>
            </section>
            <section className={styles.cards}>
                <Card
                    title="University of Florida"
                    subTitle="Web dev and maintenance"
                    className={styles.card}
                    header={<CardHeader url={'uf.png'} style={null} />}
                ></Card>
                <Card
                    title="Catamana"
                    subTitle="Software development"
                    className={styles.card}
                    header={
                        <CardHeader
                            url={'catamana.png'}
                            style={{ background: '#1288d1' }}
                        />
                    }
                ></Card>
                <Card
                    title="Pasco County"
                    subTitle="Landing pages"
                    className={styles.card}
                    header={<CardHeader url={'pasco.png'} style={null} />}
                ></Card>
                <Card
                    title="University of Texas"
                    subTitle="Marketing sites, plugins, and apps"
                    className={styles.card}
                    header={
                        <CardHeader
                            url={'ut.png'}
                            style={{ marginTop: '-30px' }}
                        />
                    }
                ></Card>
                <Card
                    title="Columbia Business School"
                    subTitle="Web development"
                    className={styles.card}
                    header={<CardHeader url={'columbia.png'} style={null} />}
                ></Card>
                <Card
                    title="FGUA"
                    subTitle="Websites and applications"
                    className={styles.card}
                    header={<CardHeader url={'fgua.png'} style={null} />}
                ></Card>
                <Card
                    title="The Nielsen Company"
                    subTitle="Internal sites and client surveys"
                    className={styles.card}
                    header={<CardHeader url={'nielsen.png'} style={null} />}
                ></Card>

                <Card
                    title="University of California, Riverside"
                    subTitle="Marketing site and web apps"
                    className={styles.card}
                    header={<CardHeader url={'ucr.png'} style={null} />}
                ></Card>
                <Card
                    title="Software Magic, Inc"
                    subTitle="Company site"
                    className={styles.card}
                    header={<CardHeader url={'smi.png'} style={null} />}
                ></Card>
            </section>
        </Layout>
    );
};

export default WithContainer(Portfolio);
