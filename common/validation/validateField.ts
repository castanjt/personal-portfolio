import * as yup from 'yup';

const validationErrorMessages = {
    name: 'Please type complete name, minimum of 2 letters.',
    message: 'Please type complete message, minimum of 25 characters.',
};

/**
 * Routes to appropriate synchronous validation function based of typeOfValidation
 * @param typeOfValidation
 * @param valueToValidate
 * @return <Object> Message property contains validation error messages or value that passed validation.
 */
export function validateFieldSync(
    typeOfValidation: string,
    valueToValidate: string,
): {
    isValid: boolean;
    message: [string];
} {
    switch (typeOfValidation) {
        case 'email':
            return validateEmailSync(valueToValidate);
        case 'name':
            return validateNameSync(valueToValidate);
        case 'message':
            return validateMessageSync(valueToValidate);
        default:
            return {
                isValid: false,
                message: ['Validation Type passed was not valid'],
            };
    }
}

/**
 * Synchronous Email validation.
 *
 * @param emailToValidate
 * @return <Object> Message property contains validation error messages or value that passed validation.
 */
function validateEmailSync(emailToValidate: string): {
    isValid: boolean;
    message: [string];
} {
    const validateEmail = yup.string().email().required();

    try {
        const result = validateEmail.validateSync(emailToValidate);
        return { isValid: true, message: [result] };
    } catch (e) {
        return { isValid: false, message: e.message };
    }
}

/**
 * Synchronous Name validation.
 *
 * @param nameToValidate
 * @return <Object> Message property contains validation error messages or value that passed validation.
 */
function validateNameSync(nameToValidate: string): {
    isValid: boolean;
    message: [string];
} {
    const errMessage = validationErrorMessages.name;
    const validateName = yup
        .string()
        .typeError(errMessage)
        .required(errMessage)
        .min(2, errMessage);

    try {
        const result = validateName.validateSync(nameToValidate);
        return { isValid: true, message: [result] };
    } catch (e) {
        return { isValid: false, message: e.message };
    }
}

/**
 * Synchronous Message validation.
 *
 * @param messageToValid
 * @return <Object> Message property contains validation error messages or value that passed validation.
 */
function validateMessageSync(messageToValid: string): {
    isValid: boolean;
    message: [string];
} {
    const errMessage = validationErrorMessages.message;
    const validateName = yup
        .string()
        .typeError(errMessage)
        .required(errMessage)
        .min(25, errMessage);

    try {
        const result = validateName.validateSync(messageToValid);
        return { isValid: true, message: [result] };
    } catch (e) {
        return { isValid: false, message: e.message };
    }
}
