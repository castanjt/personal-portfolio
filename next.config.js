module.exports = {
    target: 'serverless',
    reactStrictMode: true,
    env: {
        SITE_NAME: 'jcastanza.me',
    },
};
