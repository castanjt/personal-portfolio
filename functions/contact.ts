import * as client from '@sendgrid/mail';

function sendEmail(client, message, email, name): Promise<any> {
    return new Promise((fulfill, reject) => {
        const data = {
            to: 'jcastanza.me@gmail.com', // Change to your recipient
            from: 'jcastanza.me@gmail.com', // Change to your verified sender
            reply_to: {
                name,
                email,
            },
            subject: 'Email API: Message from jcastanza.me contact form.',
            text: message,
        };

        client
            .send(data)
            .then(([response]) => {
                fulfill(response);
            })
            .catch((error) => reject(error));
    });
}

exports.handler = function (event, context, callback) {
    const { SENDGRID_API_KEY } = process.env;

    const body = JSON.parse(event.body);
    const senderName = body.name;
    const senderMail = body.email;
    const message = body.message;

    client.setApiKey(SENDGRID_API_KEY);

    sendEmail(client, message, senderMail, senderName)
        .then((response) => callback(null, { statusCode: response.statusCode }))
        .catch((err) => callback(err, null));
};
