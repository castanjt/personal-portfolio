# Jeremy Castanza Portfolio - [jcastanza.me](https://jcastanza.me/)

This portfolio site was created with [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

After npm dependencies are installed, the site should run on `http://localhost:3000`

## Site Features

The site features five pages consisting of About, Contact, Portfolio, and Skills.

Tooling used to create the site includes:

- Next.js and React
- Sendgrid for mailing integration
- Typescript
- ESLint
- Netlify for hosting
- Primereact and SCSS for styling and useful, reusable components
## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

